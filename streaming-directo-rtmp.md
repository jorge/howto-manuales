---
title: streaming directo
---

# streaming video


## instalación servidor

según los pasos documentados por [guifi.net + exo](https://gitlab.com/guifi-exo) y explicados aqui (https://gitlab.com/guifi-exo/wiki/-/blob/master/howto/real-time-media/rtmp_streamer.md)

Usaremos el estandar [RTMP](https://en.wikipedia.org/wiki/Real-Time_Messaging_Protocol) Real-Time Messaging Protocol.


## Install rtmp mod for nginx

```bash
apt install libnginx-mod-rtmp
```

## Preparar nginx para estrimear en `rtmp`

_En este ejemplo se presume que `nginx` es el servidor web por defecto y que usa la direccón  `ejemplo.com`_

_Nginx puede estrimear codecs de HSL y Dash/MPEG, en **este ejemplo** únicamente veremos **HLS**_

en `/etc/nginx/nginx.conf` añade esto:

```
rtmp {
  access_log /var/log/nginx/rtmp-access.log;
  # abre nginx para escuchar por el puerto
  server {
    listen 1935;
    chunk_size 4096;
    # dale un nombre a tu aplicacion
    application entrada {
      live on;
      # enciende HLS
      hls on;
      hls_path /var/www/html/live.ejemplo.com/hls/;
      # ajustes de hls
      hls_fragment 3;
      hls_playlist_length 60;
      # para bloquear el directo a VLC o web
      # quita el comentario de la línea siguiente
      # deny play all;
    }
  }
}
```
## Prepara los directorios de tu web

Crea `/var/www/html/live.ejemplo.com` y los subdirectorios `hls` donde los trozos de vídeos serán almacenados:

```bash
mkdir -P /var/www/html/live.ejemplo.com/hls
```

## nginx: aplica los cambios
```bash
nginx -t
nginx -s reload
```

## Prepara tu web

Tendrás que descargar los siguientes ficheros y guardarlos en `/var/www/html/live.example.org/` :

* video.min.js  - Apache license 2 https://videojs.com/
* video-js.min.css  - Apache license 2 https://videojs.com/
* jquery-slim.min.js  - MIT license https://jquery.com/


**videojs**: iremos a https://github.com/videojs/video.js/releases, bajaremos la última versión pero utilizaremos únicamente `video-js.min.css` y `video.min.js`. Si queremos traducciones también el directorio `lang/`

**jquery** : Bajaremos la última versión minimizada desde https://jquery.com/download/ y la renombraremos `jquery-slim.min.js`


```bash
cd /tmp/
wget https://github.com/videojs/video.js/releases/download/v7.10.2/video-js-7.10.2.zip
unzip video-js-7.10.2.zip -d video-js-7.10.2
cp video-js-7.10.2/video-js.min.css video-js-7.10.2/video.min.js /var/www/html/live.ejemplo.com/
wget https://code.jquery.com/jquery-3.5.1.slim.min.js
mv /tmp/jquery-3.5.1.slim.min.js /var/www/html/live.ejemplo.com/jquery-slim.min.js
```
Crearemos `/var/www/html/live.ejemplo.com/index.html` con un contenido similar al de https://live.autistici.org/index.html

```html
<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8"/>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>live.ejemplo.com</title>
    <link rel="stylesheet" href="video-js.min.css">
    <style type="text/css">
      img { border: 0px; }
    </style>
  </head>
  <body class="">
    <main class="contain">
      <header class="">
        <h1 id="title">live.ejemplo.com</h1>
      </header>
      <content class="live-streaming">
        <video id="video" class="video-js" controls preload="auto" data-setup="{}"></video>
        <div id="instructions" style="display:none;">
          <p>
              Para crear un nuevo stream, elije un nombre único para ello y ponlo en tu sofware de emisión para que envie la señal a rtmp://live.ejemplo.com/entrada/</i> con una llave (strem key) <i>nombre_stream</i> que tu quieras.
          </p>
          <p>
            En ese momento podrás ver el directo en:
              <i>https://live.ejemplo.com/#<b>nombre_stream</b></i>.
          </p>
          <p>
              Tambien puedes apuntar tu visor de video favorito a la URL
              <i>https://live.ejemplo.com/hls/<b>nombre_stream</b>.m3u8</i>. Usa este mismo enlace para incrustar el vídeo en tu web.
          </p>
        </div>
      </content>
      <footer class="">
      </footer>
    </main>
    <script src="jquery-slim.min.js"></script>
    <script src="video.min.js"></script>
    <script>
      $(function() {
       if (window.location.hash) {
         var name = window.location.hash.substr(1);
         console.log('playing "' + name + '"');
         $('#title').text('live.ejemplo.com - ' + name);
         var v = videojs('video');
         v.src({type: 'application/x-mpegURL', src: '/hls/' + name + '.m3u8'});
       } else {
         $('#video').hide();
         $('#instructions').show();
       }
      });
    </script>
  </body>
</html>
```
Puedes crear tu propia hoja de estilos **css** y añade en `index.html`
 esta línea `<link rel="stylesheet" href="assets/css/example_com-static-app.css">` en la sección `header` tras la línea `<link rel="stylesheet" href="video-js.min.css">`:
```bash
mkdir -p assets/css/
vim /var/www/html/live.ejemplo.com/assets/css/example_com-static-app.css
```

Puedes incluir por ejemplo la anchura para el player de video:
```css
/*only for video-js and live streaming*/
@media screen {
  .contain{
    width:100vw;
    padding: 1rem;
  }
	.live-streaming .video-js, .live-streaming .video-dimensions {
		width: calc(30vw * 3);
		height: calc(15vw * 3);
	}
}
@media screen and (min-width:768px) {
  .contain{
    max-width:970px;
  }
	.live-streaming .video-js, .live-streaming .video-dimensions {
		width: calc(30vw * 2);
		max-width:970px;
		height: calc(15vw * 2);
		max-height:calc(970px / 2);
	}
}

```
## Servir la nueva web con nginx

_en este ejemplo asumimos que ya tienes los certificados creados con letsencrypt_

en el fichero `/etc/nginx/sites-available/live.ejemplo.com` añade bajo el bloque `443 ssl`:

```
    location /hls {
      # Disable cache
      add_header Cache-Control no-cache;

      # CORS setup
      add_header 'Access-Control-Allow-Origin' '*' always;
      add_header 'Access-Control-Expose-Headers' 'Content-Length';

      # allow CORS preflight requests
      if ($request_method = 'OPTIONS') {
        add_header 'Access-Control-Allow-Origin' '*';
        add_header 'Access-Control-Max-Age' 1728000;
        add_header 'Content-Type' 'text/plain charset=UTF-8';
        add_header 'Content-Length' 0;
        return 204;
      }

      types {
        application/vnd.apple.mpegurl m3u8;
        video/mp2t ts;
      }
    }
```

## Últimos pasos

### En el servidor
* controla tu firewall y permite la entrada de trafico por el puerto 1935
* haz que `/var/www/html/live.ejemplo.com/` y sus contenidos puedan ser leidos por `www-data`
* comprueba y reinicia nginx `nginx -t` y luego `nginx -s reload`


### En tu ordenador

ahora puedes instalarte [OBS](https://obsproject.com/)

`apt install obs-studio`

Envia el directo a `rtmp://live.ejemplo.com:1935/entrada/stream_key` y miralo en `https://live.ejemplo.com/#stream_key`

# Extra

##  `/etc/nginx/nginx.conf`

### configuraciones en el bloque `rtmp`

* Nginx te permite ajustar el HLS, instrucciones: https://nginx.org/en/docs/http/ngx_http_hls_module.html
* Si quieres que la gente pueda recibir el stream directamente comenta la línea "deny play all;"

* Puedes enviar el stream recibido directammente en `rtmp://live.ejemplo.com/entrada/stream_name` a un servicio externo como _youtube.com_ añadiendo `push rtmp://a.rtmp.youtube.com/live2/stream_key;`
* Puedes enviar y recibir streams simultáneamente a otros lugares o páginas web añadiendo más bloques de `aplicacones` con diferentes nombres

## Otras referencias

- https://c3voc.de/wiki/distributed-conference

## Más de un dominio en el mismo servidor

Si quieres enviar stream a más de un dominio desde el mismo servidor (ejemplo.com ejemplo2.com) y no mezclar los _streams_key_:

edita `nginx.conf`

Crea más aplicaciones bajo la sección `rtmp`:

Como nombre de la aplicación usaremos `entrada-ejemplo` and `entrada-ejemplo2` pero puedes poner lo que quieras, cada una de ellas apuntando a lugares diferentes donde el `HLS` estará disponible. Sujerimos utilizar una palabra específica para que no haya conflictos con en la aplicación.
```
# application directory for ejemplo.com
application entrada-ejemplo {
  access_log /var/log/nginx/rtmp-example-access.log;
  live on;
  # Turn on HLS
  hls on;
  hls_path /var/www/html/live.ejemplo.com/hls/;
  # finetune hls
  hls_fragment 3;
  hls_playlist_length 60;
  # Disable consuming the stream from nginx as rtmp
  deny play all;
}
# application directory for ejemplo2.com
application entrada-ejemplo2 {
  access_log /var/log/nginx/rtmp-example2-access.log;
  live on;
  # Turn on HLS
  hls on;
  hls_path /var/www/html/live.ejemplo2.com/hls/;
  # finetune hls
  hls_fragment 3;
  hls_playlist_length 60;
  # Disable consuming the stream from nginx as rtmp
  deny play all;
}
```

En Nginx edita los **sites-available** y **site-enabled** con el nuevo `ejemplo2.com`

copia la estructura de ficheros del dominio ya configurado anteriormente.

```bash
cp -r /var/www/html/live.ejemplo.com /var/www/html/live.ejemplo2.com
chown -R www-data:www-data  /var/www/html/ejemplo2.com
```
Tenemos que editar ambos `index.html` y las URL del servidor OBS para el  RMTP reemplazadno `entrada` por `entrada-ejemplo` y/o `entrada-ejemplo2`.

 ~<code>rtmp://live.ejemplo.com/entrada/</code>~

 `rtmp://live.ejemplo.com/entrada-ejemplo/`

 `rtmp://live.ejemplo2.com/entrada-ejemplo2/`


## Hardcodear el URL

Para controlar que únicamente recibe y emite en una URL mediante un `strem_key` único tenemos que cambiar esto en el `index.html`... buscar `<script>`

```js
v.src({type: 'application/x-mpegURL', src: '/hls/' + name + '.m3u8'});
```

reemplazar la salida `src:`

```js
v.src({type: 'application/x-mpegURL', src: '/hls/LOQUESEAQUEQUIERAS.m3u8'});
```

Nuestra **URL de rtmp** tiene que ser:  `rtmp://live.ejemplo.com/entrada/` y el  **stream_key** será `LOQUESEAQUEQUIERAS`

de este modo podemos dar la URL exacta a otras personas para ofrecer nuestro servicio o evitar abusos en el servirdor.

## traducción de los mensajes de _videojs_

Al pie de `index.html` o dónde tengas la llamada a

```html
<script>
  $(function() {
   if (window.location.hash) {
...

   } else {
...

   }
  });
</script>
```

añade este código _js_ dentro de `if (window.location.hash){ }`

```html
<script>
  $(function() {
   if (window.location.hash) {

//some code here ...

        videojs.addLanguage("es",{
          "You aborted the media playback": "Has interrumpido la reproducción del vídeo.",
          "A network error caused the media download to fail part-way.": "Un error de red ha interrumpido la bajada del vídeo.",
          "The media could not be loaded, either because the server or network failed or because the format is not supported.": "La emisión no está activa en este momento.",
          "The media playback was aborted due to a corruption problem or because the media used features your browser did not support.": "La reproducción del video se ha interrumpido debido a que el archivo estaba corrompido o por que tu navegador no soporta este formato.",
          "No compatible source was found for this media.": "No se ha encontrado ningún origen para este medio."
        });
        videojs.addLanguage("ca",{
          "You aborted the media playback": "Heu interromput la reproducció del vídeo.",
          "A network error caused the media download to fail part-way.": "Un error de la xarxa ha interromput la baixada del vídeo.",
          "The media could not be loaded, either because the server or network failed or because the format is not supported.": "La emissió està inactiva.",
          "The media playback was aborted due to a corruption problem or because the media used features your browser did not support.": "La reproducció de vídeo s'ha interrumput per un problema de corrupció de dades o bé perquè el vídeo demanava funcions que el vostre navegador no ofereix.",
          "No compatible source was found for this media.": "No s'ha trobat cap font compatible amb el vídeo."
        });

//some code here ...

   } else {

//some code here ...

   }
  });
</script>
```

# streaming audio

(contenido documentado según trabajo de Siroco y Mirto) puede consultarse aqui:
https://git.sindominio.net/siroco/workshop-sysadmin/src/branch/master/MUMBLE_STREAM/index.md

## Requisitos

 * Servidor o ordenador personal con entorno gráfico disponible
 * Audio del servidor o ordenador personal bajo Pulseudio
 * Las instrucciones son para usar bajo una GNU/Linux Debian 10 con los repositorio Multimedia activados

## Paqueteria

 $ sudo apt install liquidsoap mumble pavucontrol tmux

## Entorno gráfico

Arrancamos nuestra máquina y accedemos a una sesión gráfica con una usuaria sin más privilegios que el uso del audio y la red.

Arrancamos mumble

 $ mumble

Configuramos el sonido para que funcione bajo el "default" de Pulseaudio y accedemos al Mumble de Sindominio

Datos: 

 * Servidor: charlar.sindominio.net
 * Puerto: <default>
 * User: shoutcast

Accedemos a la sala que queremos streamear y dejamos el usuario allí con el Mumble encendido.

## En la terminal

Abrimos una terminal

 $ xterm

Abrimos una sesión de tmux

 $ tmux

Y creamos un fichero que llamaremos _radio.liq_ con los siguientes contenidos


```
## esto conecta la entrada por defecto del ordenador y la streamea
## El bitrate esta en 192k pero se puede bajar a 128 si vemos que no tira con la conexion
## Con buena conexion estaria bonito emitir a 320 a 256.
##
## host o servidor de stream host=""
## punto de emision, key controlado con mount=""
## 
## en este ejemplo stream.piperrak.cc:8001
## puede ser stream.espora.org:8080
##
## Requisitos
## sudo apt install liquidsoap pavucontrol

set("log.file.path","/tmp/radio.log") 
files=in()
output.icecast(%mp3(bitrate=192), files,mount="ping",password="radi0",user="source",host="stream.piperrak.cc",port=8001)
```
Guardamos y ejecutamos el script:

 $ liquidsoap -v radio.liq


Si todo va bien, vereis una linea que pone que la conexión ha sido correcta "successfull"

Si queremos dejar en background el proceso en nuestra _tmux_, por defecto, pulsamos a la vez _Ctrl+b_ y luego la letra _d_ para salir sin parar la terminal

Para acceder otra vez, _tmux a_ o a través de la sesiones disponibles usando _tmux ls_ y sabiendo el valor de la sesión, _tmux a -t <id>_

Dejo en link el tutorial que hicimos hace unos meses.

## Configuración del audio

Abrimos _pavucontrol_, el gestor de entradas y salidas de Pulseaudio

 $ pavucontrol

Y vamos a la pestaña "Grabación".

Allí encontraremos una entrada llamada "liquidsoap : pulse_in()" que por defecto se debería asignar a la entrada "Monitor Internal Audio", aunque depende de vuestra configuración de pulse y como se llamen las interfaces. 

El objetivo, hacer que liquidsoap streamee el sonido de la interfaz "Monitor" donde está funcionando también Mumble, es decir, se le llamada Monitor a la interfaz que reproduce lo que está sonando en ese momento en la computadora. Es decir, que si además del Mumble tenéis otras cosas sonando, como por ejemplo, un video en Peertube o una canción en vuestra instancia de Funkwhale, pues eso también saldrá por el streaming.  

Todo esto se podría reconfigurar a través de interfaces virtuales, pero no vamos a complicar la historia. Si algun_ está interesad_ en el asunto, hacemos otro tutorial.

## Testear que todo funciona

Si todo va bien, vamos a la dirección de nuestro streaming.

En este caso, usando piperrak.cc y el mount point "ejemplo" la dirección 

 http://stream.piperrak.cc:8001/ejemplo

Debería reproducir lo que está sonando en la sala donde tenéis a vuestra usuaria "shoutcast"

Si no funciona:

 * Revisar que teneis a la usuaria _shoutcast_ en la sala oportuna
 * Revisar la configuración de audio de Mumble
 * Si la usuaria no está, es que el cliente de mumble de _shoutcast_ está caído
 * Si no hay stream en la web del servidor de streaming, a lo mejor teneís mal configurado el script, revisar los mensajes de error que ofrece al arrancar _liquidsoap -v_

## Si no puedo acceder al entorno gráfico de una manera común

En este caso, configurar un servidor VNC, por ejemplo, **tigervnc** y lo arrancais en una usuaria que tenga privilegios de audio y red, y si tenéis bien configuradas las Xs en el server, podéis acceder en remoto a Mumble y arrancarlo con el usuario y pulseaudio.

Si no, podéis dejar una maquina que no estéis usando para la charla/streaming haciendo este trabajo, sin complicaros con configurar todo esto en un servidor sin Xs. Incluso, con una Tails o alguna Live con Debian debería ser posible ocnfigurar todo esto sin mucha complicación.

## Links

 * https://www.mumble.info/
 * https://wiki.archlinux.org/index.php/PulseAudio/Examples
 * https://www.liquidsoap.info/doc-dev/reference.html
 * https://www.linuxfordevices.com/tutorials/debian/install-vnc-server-on-debian
 * https://git.sindominio.net/siroco/workshop-sysadmin/src/branch/master/TMUX/index.md

